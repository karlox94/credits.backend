<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->options('/{any:.*}',function() {});

$router->group([
	'prefix' => '/api/v1/',
	'middleware' => ['cors']
], function ($app) {
    $app->get('/user/{identification}','UserController@get');
    $app->post('/user/store','UserController@store');
    
    $app->post('/request/store','RequestController@store');
});
