<?php

namespace App\Http\Controllers;

use App\Request as Req;
use Illuminate\Http\Request;
use DateTime;

class RequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $today = new DateTime();
        $dateHire = new DateTime($data['date_hire']);
        $days = $today->diff($dateHire)->days;
        
        if ($days < 547) {
            $data['amount'] = 0;
            $data['state'] = 0;
            $R = Req::create($data);
            return response()->json([$R, 1]);
        } else if ($data['salary'] < 800000) {
            $data['amount'] = 0;
            $data['state'] = 0;
            $R = Req::create($data);
            return response()->json([$R, 2]);
        } else if ($data['salary'] > 800000 && $data['salary'] < 1000000) {
            $data['amount'] = 5000000;
            $data['state'] = 1;
            $R = Req::create($data);
            return $R;
        } else if ($data['salary'] > 1000000 && $data['salary'] < 4000000) {
            $data['amount'] = 20000000;
            $data['state'] = 1;
            $R = Req::create($data);
            return $R;
        } else if ($data['salary'] > 4000000) {
            $data['amount'] = 50000000;
            $data['state'] = 1;
            $R = Req::create($data);
            return $R;
        }
    }

    public function get($identification)
    {
        $R = User::where('identification', $identification)->with('requests')->get();
        return $R;
    }
}
