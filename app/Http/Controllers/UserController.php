<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request)
    {
        $User = User::create($request->all());
        return $User;
    }

    public function get($identification)
    {
        $User = User::where('identification', $identification)->get();
        return $User;
    }
}
